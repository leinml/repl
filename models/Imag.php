<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class Imag extends Model
{
   
    public $imagefile;

    public function rules()
    {
        return [
            [['imagefile'], 'file', 'skipOnEmpty' => false,'uploadRequired'=>'No se puede dejar vacio'],
             [['imagefile'], 'file', 'extensions' => 'png,jpg,jpeg','wrongExtension'=>'Solo se pueden subir imagenes con extension .png .jpg .jpeg'],
        ];
    }
    
    
}