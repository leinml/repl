<?php

namespace app\models;

use yii\db\ActiveRecord;

class Form extends ActiveRecord
{


	public function rules(){

		return [

			[['names','last','email'],'required','message'=>'No se puede dejar vacio'],

			['email', 'email','message'=>'No es un email valido'],


			[['names','last','email','webpage','city'],'string','message'=>'No es valido'],

			[['phone','cellphone'],'integer','message'=>'Debe ingresar un numero'],

			 ['webpage', 'url', 'message' => 'No es valido'],



		];


	}
    
}