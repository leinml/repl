<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Menu;
use yii\helpers\Url;
use app\models\Category;

use \kartik\depdrop\DepDrop;

$this->title = 'Busqueda';




$form = ActiveForm::begin([
        'action' => ['show'],
        'method' => 'get',
    ]) ?>

	<div class="footer">

    	 <?=  $form->field($search, 'items1')->dropdownList(['atr1'=>'Categoria 1','atr2'=>'Categoria 2','atr3'=>'Categoria 3'
              
             ],['prompt'=>'Seleccionar','id'=>'cat-id','class'=>'input-large form-control'])->label('Categoria 1');?>
          
         <?= $form->field($search, 'items2')->widget(DepDrop::classname(), [
    'options'=>['id'=>'subcat-id', 'class'=>'input-large form-control'],
    'pluginOptions'=>[
        'depends'=>['cat-id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['site/item'])
    ]
]);
    ?>
    	
    
	</div>
    <div class="form-group">
        <div >
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>


