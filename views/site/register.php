<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;

use \kartik\depdrop\DepDrop;

$this->title = 'Registrarse';

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

	<div class="footer">

    	<?= $form->field($model, 'names')->label('Nombres')->textInput(['maxlength'=>20]); ?>
    	<?= $form->field($model, 'last')->label('Apellidos')->textInput(['maxlength'=>20]); ?>
    	<?= $form->field($model, 'email')->label('Email')->textInput(['maxlength'=>30]); ?>
    	<?= $form->field($model, 'adress')->label('Direccion')->textInput(['maxlength'=>40]); ?>
    	<?= $form->field($model, 'phone')->label('Telefono')->textInput(['maxlength'=>20]); ?>
    	<?= $form->field($model, 'cellphone')->label('Celular')->textInput(['maxlength'=>20]); ?>
    	<?= $form->field($model, 'webpage')->label('Pagina web')->textInput(['maxlength'=>20]); ?>
    	<?= $form->field($model, 'city')->label('Ciudad')->textInput(['maxlength'=>20]); ?>

    	

    	<?= $form->field($model, 'description')->label('Descripcion')->textInput(['style'=>'height:200px','maxlength'=>240]); ?>
        <?=Html::label("Imagen");   ?>

       	<?= $form->field($image, 'imagefile')->fileInput([ 'class' => 'sr-only'])->label('Buscar una imagen ...',['class'=>'btn btn-default ']); ?>

         <?=  $form->field($search, 'items1')->dropdownList(['atr1'=>'Categoria 1','atr2'=>'Categoria 2','atr3'=>'Categoria 3'
              
             ],['prompt'=>'Seleccionar','id'=>'cat-id','class'=>'input-large form-control'])->label('Categoria 1');?>
          
         <?= $form->field($search, 'items2')->widget(DepDrop::classname(), [
    'options'=>['id'=>'subcat-id', 'class'=>'input-large form-control'],
    'pluginOptions'=>[
        'depends'=>['cat-id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['site/item'])
    ]
]);
    ?>
    
	</div>
    <div class="form-group">
        <div >
            <?= Html::submitButton('Crear', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>