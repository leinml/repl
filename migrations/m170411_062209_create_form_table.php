<?php

use yii\db\Migration;


class m170411_062209_create_form_table extends Migration
{
    function up()
    {
        $this->createTable('form', [

            
            
            'names'=>$this->string(20)->notnull(),
            'last'=>$this->string(20)->notnull(),
            'email'=>$this->string(30)->notnull(),
            'adress'=>$this->string(40),
            'phone'=>$this->integer(20),
            'cellphone'=>$this->integer(20),
            'webpage'=>$this->string(20),
            'city'=>$this->string(20),
            'description'=>$this->text(140),

            'extension'=>$this->string(4),

            'category'=>$this->string(10),

        ]);

        $this->addColumn(
        'form',
        'id',
        'INT(9) UNSIGNED NOT NULL AUTO_INCREMENT primary key FIRST');


       

    }

   
    public function down()
    {
        $this->dropTable('form');
        
    }
}
