<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Form;

use app\models\Imag;

use app\models\Item;

use app\models\Search;

use yii\web\UploadedFile;

use yii\data\Pagination;

use yii\imagine\Image;

use app\models\Subclass;

use   yii\helpers\Json;




class SiteController extends Controller
{
   
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionItem(){

             $out = [];
        
            $id = $_POST['depdrop_parents'][0];

            switch ($id) {
              case 'atr1':
                  $out =  [
               ['id'=>'atr1', 'name'=>'<sub-cat-name1>'],
                ['id'=>'atr2', 'name'=>'<sub-cat-name2>'],
                ['id'=>'atr3', 'name'=>'<sub-cat-name3>']
             ];
                break;

              case 'atr2':
                $out =  [
                 ['id'=>'atr4', 'name'=>'<sub-cat-name4>'],
                  ['id'=>'atr5', 'name'=>'<sub-cat-name5>'],
                  ['id'=>'atr6', 'name'=>'<sub-cat-name6>']
               ];
                
                break;

              case 'atr3':

                $out =  [
               ['id'=>'atr7', 'name'=>'<sub-cat-name7>'],
                ['id'=>'atr8', 'name'=>'<sub-cat-name8>'],
                ['id'=>'atr9', 'name'=>'<sub-cat-name9>']
             ];
                
                break;
              
             
            }
            
                
                
                 echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            
        
       

    }

   
    public function actionIndex()
    {

       

        return $this->render('index');
    }



    public function actionShow(){


      $search=$_GET['Search']['items2'];

      

         

         

        $query= Form::find()->where(['category' =>$search]);


        $count=$query->count();


         $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $count,
        ]);



         $list=$query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();


         

              return $this->render('show',['list'=>$list,'pagination'=>$pagination]);

            

            

    }


    public function actionSearch(){

      


       $search= new Search();

        return $this->render('search',['search'=>$search]);

    }

   
  

    public function actionRegister(){

        $model = new Form();

        $image = new Imag();

          $search= new Search();


           $image->imagefile =UploadedFile::getInstance($image, 'imagefile');


        if(Yii::$app->request->post() ){

            $model->names=$_POST['Form']['names'];
            $model->last=$_POST['Form']['last'];
             $model->email=$_POST['Form']['email'];
              $model->adress=$_POST['Form']['adress'];
            $model->phone=$_POST['Form']['phone'];
             $model->cellphone=$_POST['Form']['cellphone'];
              $model->webpage=$_POST['Form']['webpage'];
            $model->city=$_POST['Form']['city'];
             $model->description=$_POST['Form']['description'];


           


           

            

               $model->extension=$image->imagefile->extension;

               $model->save();

             $image->validate();

             $image->imagefile->saveAs('images/' . $model->id. '.' . $image->imagefile->extension);

             Image::thumbnail('images/'.$model->id.'.'.$image->imagefile->extension, 240, 240)
    ->save('images/'.$model->id.'.'.$image->imagefile->extension, ['quality' => 80]);


          $model->category=$_POST['Search']['items2'];


           

           
          $model->save();

            return $this->render('index');

        }




        return $this->render('register',['model'=>$model,'image'=>$image,'search'=>$search]);

    }
   
}
